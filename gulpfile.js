var gulp = require('gulp'),
    chalk = require('chalk'),
    figlet = require('figlet'),
    emoji = require('node-emoji');

/* gnna-task-sass */    
    gulp.task('sass', function () {
            
        console.log(chalk.blue(emoji.get('heart_eyes')));
        console.log(chalk.green(emoji.get('heavy_check_mark')));
        console.log( correto );
        console.log(chalk.hex('#ff0').bgHex('#121679').bold('www.gabenna.io | sass run 45'));
            //return gulp.src('./src/styles/**/*.scss')
        console.log("correção do scss para coletar os sub-componentes");
        
        return gulp.src([
            './src/styles/**/*.scss', 
            './src/styles/**/**/*.scss'
        ])
            //.pipe(sassLint())
            //.pipe(sassLint.format())
            //.pipe(sassLint.failOnError())
        .pipe(sass().on('error', sass.logError))
            //.pipe(rename('./css/bio-styles.min.css'))
            .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
            }))
            .pipe(cssmin())
            .pipe(gulp.dest('./dist'))
            .on('error', err => {
            console.log("deu ruim ao gerar css");
            console.log(chalk.blue(emoji.get('heart_eyes')));
            console.log(figlet.textSync("sass up!"));
            console.log(chalk.hex('#ff0').bgHex('#121679').bold('www.gabenna.io sass run'));
            });
            
    });

/* gnna-task-webpack-babel */
    gulp.task('webpack', function() {
        return gulp.src('src/gnnaio.js')
        .pipe(webpack({
            config : require('./webpack.config.js')
        }))
        .pipe(gulp.dest('dist/'));
    });

/* gnna-task-webpack-babel */
